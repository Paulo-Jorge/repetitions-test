#!/usr/bin/env python
#

from Vector3D import *
import unittest


def _tolerance(f1, f2, tol=1.0e-8):
    return abs(f1-f2) < tol


class Vector3DTestCase(unittest.TestCase):
    """A simple and incomplete test case for Vector3D"""
    def setUp(self):
        self.v1 = Vector3D(1.,2.,3.)
        self.v2 = Vector3D(5.,7.,9.)
        self.v3 = self.v1+self.v2


    def testAccessors(self):
        assert (self.v1[0] == 1. and self.v1[1] == 2.0 and self.v1[2] == 3.0), "Constructor/Indexing"
        assert (self.v2.x() == 5. and self.v2.y() == 7.0 and self.v2.z() == 9.0), "Constructor/Accessor"
        assert self.v3 == Vector3D(6.,9.,12.), "Constructor/Addition/Comparison"


    def testBuiltins(self):
        assert repr(self.v1) == "Vector3D(1.0,2.0,3.0)", "repr() failed"
        assert str(self.v2) == "[5.0, 7.0, 9.0]", "str() failed"
        assert len(self.v3) == 3, "len() failed"


    def testMaths(self):
        assert -self.v3 == Vector3D(-6.,-9.,-12.), "negate"
        assert self.v3+self.v3 == 2.0*self.v3, "addition and multiply by scalar"
        assert self.v2-self.v3 == Vector3D(-1.,-2.,-3.), "Subtraction operator"
        assert self.v1*self.v2 == 46.0, "Dot product"
        assert self.v1*self.v1 == 14.0, "Dot product"

        result = Vector3D(15.,21.,27.)
        assert 3.0*self.v2 == result, "Scalar*Vector"
        assert self.v2*3.0 == result, "Vector*Scalar"
        assert self.v2/3.0 == Vector3D(5.0/3.0,7.0/3.0,9.0/3.0), "Vector/Scalar"
        try:
            result = 3.0/self.v2
        except TypeError:
            pass
        else:
            fail("expected a TypeError")


    def testMethods(self):
        result = Vector3D(-3.0, 6.0, -3.0)
        assert self.v1.cross(self.v2) == result, "v1.cross(v2)/ __cmp__()"
        assert _tolerance(self.v1.length(), 3.74165738677), "v1.length()"
        assert _tolerance(self.v1.angle(self.v2), 0.158410840631), "v1.angle(v2)"

        result = Vector3D(0.2672612419, 0.53452248382, 0.80178372573)
        assert self.v1.normal().equal(result), "v1.normal()/ equal()"

        assert self.v1 == Vector3D(1.,2.,3.), "v1 changed"
        assert self.v2 == Vector3D(5.,7.,9.), "v2 changed"

    
    def testConstants(self):
        assert Vector3D.zero == Vector3D(0.,0.,0.), "bad constant"
        assert Vector3D.one  == Vector3D(1.,1.,1.), "bad constant"
        assert Vector3D.ex   == Vector3D(1.,0.,0.), "bad constant"
        assert Vector3D.ey   == Vector3D(0.,1.,0.), "bad constant"
        assert Vector3D.ez   == Vector3D(0.,0.,1.), "bad constant"

        assert isVector3D(self.v2) == 1, "isVector3D(v2)"
        assert isVector3D((1.,2.,3.)) == 0, "isVector3D( (1.,2.,3.) )"
        assert isVector3D( 1.0 )   == 0, "isVector3D( 1.0 )"


def suite():
    return unittest.makeSuite(Vector3DTestCase)


if __name__ == '__main__':
    unittest.main()

