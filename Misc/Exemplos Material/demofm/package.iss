;
; This script needs the InnoSetup PreProcessor (ISPP) to compile correctly. 
; I downloaded a combined 'QuickStart Pack' from here:
; http://www.jrsoftware.org/isdl.php#qsp
;
; The version string is extracted from the executable.
;

#include "config.iss"

#define MacroPublisherURL "{code:MyPublisherURL}"
#define MacroReadme       "{code:MyReadme}"
#define MacroLicence      "{code:MyLicence}"

[Setup]
AppName={#AppName}
AppVerName={#AppName} {#AppVersion}
AppPublisher={#AppPublisher}
AppPublisherURL={#MacroPublisherURL}
AppSupportURL={#MacroPublisherURL}
AppUpdatesURL={#MacroPublisherURL}
DefaultDirName={pf}\{#AppDirName}
DefaultGroupName={#AppGroupName}
LicenseFile={#AppLicence}
OutputDir={#AppOutputDir}
OutputBaseFilename={#AppPrefix}-{#AppVersion}-{#AppSuffix}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "eng"; MessagesFile: "compiler:Default.isl"
Name: "bra"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl";
Name: "cat"; MessagesFile: "compiler:Languages\Catalan.isl";
Name: "cze"; MessagesFile: "compiler:Languages\Czech.isl";
Name: "dan"; MessagesFile: "compiler:Languages\Danish.isl";
Name: "dut"; MessagesFile: "compiler:Languages\Dutch.isl";
Name: "fre"; MessagesFile: "compiler:Languages\French.isl";
Name: "ger"; MessagesFile: "compiler:Languages\German.isl";
Name: "hun"; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: "ita"; MessagesFile: "compiler:Languages\Italian.isl";
Name: "nor"; MessagesFile: "compiler:Languages\Norwegian.isl"
Name: "pol"; MessagesFile: "compiler:Languages\Polish.isl";
Name: "por"; MessagesFile: "compiler:Languages\Portuguese.isl"
Name: "rus"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "slo"; MessagesFile: "compiler:Languages\Slovenian.isl"

; Additional, Candidate official translations
Name: "chs"; MessagesFile: "compiler:Languages\ChineseSimp-11-5.1.0.isl"

; Additional, Unofficial translations
Name: "esp"; MessagesFile: "compiler:Languages\SpanishStd-2-5.1.0.isl";
Name: "gal"; MessagesFile: "compiler:Languages\Galician-2-5.1.0.isl"
Name: "cht"; MessagesFile: "compiler:Languages\ChineseTrad-2-5.1.0.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "{#BdistDir}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs

[INI]
Filename: "{app}\{#AppName}.url"; Section: "InternetShortcut"; Key: "URL"; String: "{#MacroPublisherURL}"

[Icons]
Name: "{code:MyGroupDir}\{#AppGroupName}\{#AppName} (Windowed)";    Filename: "{app}\{#AppExe}";                              Comment: "Start {#AppName} in Windowed mode"
Name: "{code:MyGroupDir}\{#AppGroupName}\{#AppName} (Full Screen)"; Filename: "{app}\{#AppExe}"; Parameters: "--fullscreen";  Comment: "Start {#AppName} in Full-Screen mode"
Name: "{code:MyGroupDir}\{#AppGroupName}\Readme"; Filename: "{app}\{#MacroReadme}"; Comment: "View ReadMe"
Name: "{code:MyGroupDir}\{#AppGroupName}\Licence"; Filename: "{app}\{#MacroLicence}"; Comment: "View License"
Name: "{code:MyGroupDir}\{#AppGroupName}\{cm:ProgramOnTheWeb,{#AppName}}"; Filename: "{app}\{#AppName}.url"; Comment: "{cm:ProgramOnTheWeb,{#AppName}}"
Name: "{code:MyGroupDir}\{#AppGroupName}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"; Comment: "Remove {#AppName}"
Name: "{code:MyDesktopDir}\{#AppName}"; Filename: "{app}\{#AppExe}"; Tasks: desktopicon

[Registry]
Root: HKLM; Subkey: "SOFTWARE\{#AppRegKey}"; Flags: uninsdeletekey; ValueName: "{#AppRegValue}"; ValueType: string; ValueData: "{app}"; Check: AllUsers;
Root: HKCU; Subkey: "SOFTWARE\{#AppRegKey}"; Flags: uninsdeletekey; ValueName: "{#AppRegValue}"; ValueType: string; ValueData: "{app}"; Check: ThisUserOnly;
Root: HKLM; Subkey: "SOFTWARE\{#AppRegKey}"; Flags: uninsdeletekey; ValueName: "{#AppRegVersion}"; ValueType: string; ValueData: "{#AppVersion}"; Check: AllUsers;
Root: HKCU; Subkey: "SOFTWARE\{#AppRegKey}"; Flags: uninsdeletekey; ValueName: "{#AppRegVersion}"; ValueType: string; ValueData: "{#AppVersion}"; Check: ThisUserOnly;

[Run]
Filename: "{app}\{#MacroReadme}"; Description: "View the README file"; Flags: postinstall shellexec

[UninstallDelete]
Type: files; Name: "{app}\{#AppName}.url"

[code]
const
  CSIDL_PROFILE = $0028;
  CSIDL_COMMON_PROGRAMS = $0017;
  CSIDL_COMMON_DESKTOPDIRECTORY = $0019;
var
  InstallTypePageID: Integer;
  CheckListBox2: TNewCheckListBox;

function Restricted(): Boolean;
begin
  Result := not (IsAdminLoggedOn() or IsPowerUserLoggedOn())
end;

function NotRestricted(): Boolean;
begin
  Result := not Restricted()
end;

function Is9xME(): Boolean;
begin
  Result := not UsingWinNT()
end;

function CurrentUserOnly(): Boolean;
begin
  Result := CheckListBox2.Checked[2]
end;

function ThisUserOnly(): Boolean;
begin
  Result := (Restricted() or CurrentUserOnly()) and UsingWinNT()
end;

function AllUsers(): Boolean;
begin
  Result := not ThisUserOnly()
end;

function MyAppDir(): String;
var
  Path: String;
begin
  Path := ExpandConstant('{reg:HKLM\SOFTWARE\{#AppRegKey},{#AppRegValue}|{pf}\{#AppDirName}}');
  if ThisUserOnly() then
  begin
    Path := ExpandConstant('{reg:HKCU\SOFTWARE\{#AppRegKey},{#AppRegValue}|__MissingKey__}');
    if Path = '__MissingKey__' then
    begin
      Path := GetShellFolderByCSIDL(CSIDL_PROFILE, True);
      if Path = '' then
        Path := RemoveBackslashUnlessRoot(ExtractFilePath(ExpandConstant('{userdocs}')));
      Path := Path + '\Programs\{#AppDirName}'
    end
  end
  Result := Path
end;

function MyGroupDir(Default: String): String;
var
  Path: String;
begin
  if ThisUserOnly() then
    Path := ExpandConstant('{userprograms}')
  else
    Path := ExpandConstant('{commonprograms}')
  Result := Path
end;

function MyDesktopDir(Default: String): String;
var
  Path: String;
begin
  if ThisUserOnly() then
    Path := ExpandConstant('{userdesktop}')
  else
    Path := ExpandConstant('{commondesktop}')
  Result := Path
end;

procedure CreateTheWizardPages;
var
  Page: TWizardPage;
  Enabled, InstallAllUsers: Boolean;
begin
  Page := CreateCustomPage(wpLicense, 'Choose Installation Type', 'Who do you want to be able to use this program?');
  InstallTypePageID := Page.ID;
  Enabled := NotRestricted();
  InstallAllUsers := NotRestricted();
  CheckListBox2 := TNewCheckListBox.Create(Page);
  CheckListBox2.Width := Page.SurfaceWidth;
  CheckListBox2.Height := ScaleY(97);
  CheckListBox2.BorderStyle := bsNone;
  CheckListBox2.ParentColor := True;
  CheckListBox2.MinItemHeight := WizardForm.TasksList.MinItemHeight;
  CheckListBox2.ShowLines := False;
  CheckListBox2.WantTabs := True;
  CheckListBox2.Parent := Page.Surface;
  CheckListBox2.AddGroup('Installation Type:', '', 0, nil);
  CheckListBox2.AddRadioButton('All Users', '', 0, InstallAllUsers, Enabled, nil);
  CheckListBox2.AddRadioButton('Current User Only', '', 0, not InstallAllUsers, True, nil);
end;

procedure CurPageChanged(CurPageID: Integer);
begin
  if CurPageID = wpSelectDir then
  begin
    WizardForm.DirEdit.Text := MyAppDir();
  end
end;

function ShouldSkipPage(PageID: Integer): Boolean;
begin
  Result := (PageID = InstallTypePageID) and Is9xME();
end;

procedure InitializeWizard();
begin
  CreateTheWizardPages;
end;

function MyReadme(Default: String): String;
begin
  Result := '{#AppReadme}'
end;

function MyLicence(Default: String): String;
begin
  Result := '{#AppLicence}'
end;

function MyPublisherURL(Default: String): String;
begin
  Result := '{#AppPublisherURL}'
end;

#expr SaveToFile(AddBackslash(SourcePath) + 'Preprocessed.iss')

