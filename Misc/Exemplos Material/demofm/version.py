##############################################################################
#
#   $RCSfile: version.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: version.py,v 1.1 2006/06/25 02:04:04 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.1 $"

config = {
    "APP_TITLE"         : "Demo Fruit Machine",
    "APP_PREFIX"        : "DemoFM",
    "PKG_PREFIX"        : "demo-fm",
    "APP_SUFFIX"        : "win32-installer",
    "APP_VERSION"       : "0.0.1",
    "APP_ICON"          : "3dbuilder.ico",
    "APP_README"        : "readme.txt",
    "APP_PUBLISHER"     : "John Popplewell",
    "APP_PUBLISHER_URL" : "http://www.johnnypops.demon.co.uk/",
    "APP_LICENCE"       : "copying.txt",
}

config["APP_GROUPNAME"] = config["APP_TITLE"]

