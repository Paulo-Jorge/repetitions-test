##############################################################################
#
#   $RCSfile: reels.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: reels.py,v 1.2 2006/06/25 02:02:48 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.2 $"

import math
import random
import oglutils
import pygutils

from Vector3D  import *
from OpenGL.GL import *
from operator  import truth

#
########################################################################
#

GOLDEN_NUMBER	=	((math.sqrt(5.0)-1.0)/2.0)
GOLDEN_ANGLE	=	(2.0*math.pi*(1.0-GOLDEN_NUMBER))

NUM_REELS       =   4

# Reel picture indexes

FT_CHERRY   =   0
FT_LEMON    =   1
FT_APPLE    =   2
FT_PEAR     =   3
FT_3BAR     =   4
FT_BELL     =   5
FT_PLUM     =   6
FT_ORANGE   =   7
FT_GRAPE    =   8
FT_MELON    =   9

# Attribute indexes

NM_NONE     =   0
AT_NONE     =   0

# Unique symbols

TEX_CHERRY  =   (FT_CHERRY, NM_NONE , AT_NONE  , "cherry.png")
TEX_LEMON   =   (FT_LEMON , NM_NONE , AT_NONE  , "lemon.png")
TEX_APPLE   =   (FT_APPLE , NM_NONE , AT_NONE  , "apple.png")
TEX_PEAR    =   (FT_PEAR  , NM_NONE , AT_NONE  , "pear.png")
TEX_3BAR    =   (FT_3BAR  , NM_NONE , AT_NONE  , "3bar.png")
TEX_BELL    =   (FT_BELL  , NM_NONE , AT_NONE  , "bell.png")
TEX_PLUM    =   (FT_PLUM  , NM_NONE , AT_NONE  , "plum.png")
TEX_ORANGE  =   (FT_ORANGE, NM_NONE , AT_NONE  , "orange.png")
TEX_GRAPE   =   (FT_GRAPE , NM_NONE , AT_NONE  , "grape.png")
TEX_MELON   =   (FT_MELON , NM_NONE , AT_NONE  , "melon.png")

# The reels

REEL = [
    (
    TEX_CHERRY,TEX_LEMON,TEX_APPLE,TEX_PEAR,  TEX_APPLE,TEX_3BAR, TEX_BELL, TEX_APPLE,
    TEX_PEAR,  TEX_PLUM, TEX_APPLE,TEX_ORANGE,TEX_PEAR, TEX_APPLE,TEX_LEMON,TEX_APPLE,
    TEX_GRAPE, TEX_PEAR, TEX_APPLE,TEX_PEAR,  TEX_3BAR, TEX_APPLE,TEX_MELON,TEX_3BAR
    ),
    (
    TEX_CHERRY,TEX_GRAPE, TEX_PEAR, TEX_ORANGE,TEX_LEMON, TEX_GRAPE, TEX_ORANGE,TEX_GRAPE,
    TEX_PLUM,  TEX_ORANGE,TEX_APPLE,TEX_LEMON, TEX_ORANGE,TEX_3BAR,  TEX_ORANGE,TEX_LEMON,
    TEX_3BAR,  TEX_ORANGE,TEX_3BAR, TEX_LEMON, TEX_APPLE, TEX_ORANGE,TEX_MELON, TEX_BELL
    ),
    (
    TEX_CHERRY,TEX_LEMON,TEX_APPLE, TEX_LEMON, TEX_PEAR, TEX_ORANGE,TEX_LEMON,TEX_BELL,
    TEX_LEMON, TEX_GRAPE,TEX_3BAR,  TEX_ORANGE,TEX_LEMON,TEX_3BAR,  TEX_LEMON,TEX_3BAR,
    TEX_PLUM,  TEX_LEMON,TEX_ORANGE,TEX_PLUM,  TEX_LEMON,TEX_MELON, TEX_LEMON,TEX_PLUM
    ),
    (
    TEX_CHERRY,TEX_MELON,TEX_PEAR,TEX_APPLE,TEX_PEAR, TEX_ORANGE,TEX_3BAR,  TEX_PEAR,
    TEX_3BAR,  TEX_APPLE,TEX_PEAR,TEX_APPLE,TEX_3BAR, TEX_PEAR,  TEX_ORANGE,TEX_PEAR,
    TEX_APPLE, TEX_GRAPE,TEX_PLUM,TEX_PEAR, TEX_APPLE,TEX_BELL,  TEX_APPLE, TEX_LEMON
    )
]

# Win sfx mappings

SND_WIN_MEGA        = "whoooo.wav"
SND_WIN             = "Brassstab2.wav"
SND_1ANY_WIN        = "oohhh.wav"

SND_ALL_CHERRY_WIN	= SND_WIN_MEGA
SND_ALL_LEMON_WIN	= SND_WIN_MEGA
SND_ALL_APPLE_WIN	= SND_WIN_MEGA
SND_ALL_PEAR_WIN	= SND_WIN_MEGA
SND_ALL_3BAR_WIN	= SND_WIN_MEGA
SND_ALL_BELL_WIN	= SND_WIN_MEGA
SND_ALL_PLUM_WIN	= SND_WIN_MEGA
SND_ALL_ORANGE_WIN	= SND_WIN_MEGA
SND_ALL_GRAPE_WIN	= SND_WIN_MEGA
SND_ALL_MELON_WIN	= SND_WIN_MEGA

SND_3CHERRY_WIN		= SND_WIN
SND_3LEMON_WIN		= SND_WIN
SND_3APPLE_WIN		= SND_WIN
SND_3PEAR_WIN		= SND_WIN
SND_33BAR_WIN		= SND_WIN
SND_3BELL_WIN		= SND_WIN
SND_3PLUM_WIN		= SND_WIN
SND_3ORANGE_WIN		= SND_WIN
SND_3GRAPE_WIN		= SND_WIN
SND_3MELON_WIN		= SND_WIN

SND_2ANY_WIN		= SND_1ANY_WIN
SND_3ANY_WIN		= SND_1ANY_WIN

# Win Rules

NINA_ROW = [                                            #"N" Consecutive
    (FT_CHERRY, NUM_REELS, 1000, SND_ALL_CHERRY_WIN),
    (FT_BELL  , NUM_REELS, 1000, SND_ALL_BELL_WIN),
    (FT_MELON , NUM_REELS, 1000, SND_ALL_MELON_WIN),
    (FT_GRAPE , NUM_REELS, 800 , SND_ALL_GRAPE_WIN),
    (FT_PLUM  , NUM_REELS, 800 , SND_ALL_PLUM_WIN),
    (FT_PEAR  , NUM_REELS, 700 , SND_ALL_PEAR_WIN),
    (FT_ORANGE, NUM_REELS, 600 , SND_ALL_ORANGE_WIN),
    (FT_3BAR  , NUM_REELS, 500 , SND_ALL_3BAR_WIN),
    (FT_LEMON , NUM_REELS, 500 , SND_ALL_LEMON_WIN),
    (FT_APPLE , NUM_REELS, 400 , SND_ALL_APPLE_WIN),

    (FT_CHERRY,         3, 300 , SND_3CHERRY_WIN),
    (FT_MELON ,         3, 300 , SND_3MELON_WIN),
    (FT_BELL  ,         3, 300 , SND_3BELL_WIN),
    (FT_3BAR  ,         3, 200 , SND_33BAR_WIN),
    (FT_PLUM  ,         3, 200 , SND_3PLUM_WIN),
    (FT_GRAPE ,         3, 200 , SND_3GRAPE_WIN),
    (FT_ORANGE,         3, 120 , SND_3ORANGE_WIN),
    (FT_PEAR  ,         3, 100 , SND_3PEAR_WIN),
    (FT_LEMON ,         3,  80 , SND_3LEMON_WIN),
    (FT_APPLE ,         3,  60 , SND_3APPLE_WIN)
]


ANY_WIN = [                                             #"ANY" position
    (FT_CHERRY, 3, 120, SND_3ANY_WIN),
    (FT_CHERRY, 2,  80, SND_2ANY_WIN),
    (FT_CHERRY, 1,  40, SND_1ANY_WIN)
]

#
########################################################################
#

class RuleBase:
    def __init__(self, fruit, prize, sound ):
        self.fruit = fruit
        self.prize = prize
        self.sound = pygutils.sndman.getSound( sound )

    def getFruit(self):
        return self.fruit

    def getPrize(self):
        return self.prize

    def getTotalPrize(self):
        return self.prize

    def getStream(self):
        return self.sound


class NinaRow(RuleBase):
    def __init__(self, fruit, NinaRow, prize, sound ):
        RuleBase.__init__(self, fruit, prize, sound)
        self.NinaRow = NinaRow

    def getWin(self, winline):
        numReels = len(winline)
        i = 0
        while i < (numReels-self.NinaRow+1):
            if winline[i] != self.fruit:
                i += 1
                continue
            count, j = 1, i+1
            while j < numReels:
                if winline[j] != self.fruit: break
                count += 1
                j += 1
            if count == self.NinaRow: return 1
            i = j+1
        return 0

    def getWin1(self, winline):
        if self.NinaRow == 4:
            return winline.count(self.fruit) == self.NinaRow
        if winline[0] == self.fruit and winline[1] == self.fruit and winline[2] == self.fruit:
	    return 1
	if winline[1] == self.fruit and winline[2] == self.fruit and winline[3] == self.fruit:
	    return 1
        return 0

    def getHold(self):
        pass


class AnyWin(RuleBase):
    def __init__(self, fruit, N, prize, sound ):
        RuleBase.__init__(self, fruit, prize, sound)
        self.N = N

    def getWin(self, winline):
        return winline.count(self.fruit) == self.N

    def getHold(self):
        pass

import UserList

class RuleList(UserList.UserList):
    def __init__(self, numReels, numFruit):
        UserList.UserList.__init__(self)
        self.prizes = []
        self.numReels = numReels
        self.minHoldOdds = 1.0/numFruit

        for rule in NINA_ROW:
            self.append( apply(NinaRow, rule) )
        for rule in ANY_WIN:
            self.append( apply(AnyWin, rule) )

        for rule in self.data:
            prize = rule.getTotalPrize()
            if not (prize in self.prizes):
                self.prizes.append(prize)

        self.prizes.sort()

    def getNPrizes(self):
        return len(self.prizes)

    def getMaxPrize(self):
        return self.prizes[self.getNPrizes()-1]

    def getMinPrize(self):
        return self.prizes[0]

    def getPrizeAt(self, idx):
        return self.prizes[idx]

    def getPrizeList(self):
        return self.prizes

    def getWinIndex(self, win):
        for index in range(len(self.prizes)):
            if self.prizes[index] == win: return index

        print "INTERNAL ERROR : Win not found in Win table."
        return 0;

    def findWin(self, winline):
        win = 0
        result = None
        for rule in self.data:
            if rule.getWin(winline) and rule.getTotalPrize() > win:
                win = rule.getTotalPrize()
                result = rule
        return result

    def findHold(self):
        pass

#
########################################################################
#

def Deg2Rad( a ): return a*math.pi/180.0

def Rad2Deg( a ): return a*180.0/math.pi

def Srnd( f ): return random.uniform( -f/2, f/2 )

def Bool(i): return not not i


class Reel:
    def __init__(self, parent, origin, index):
        self.parent = parent
        self.origin = origin
        self.index = index
        self.info = REEL[index]

        self.rev = 1.0
        self.pos = 0.0
        self.fruitindex = 0
        self.action = self.__stopped

        self.holdbutton = None
        self.holdOn = 0

        for fruit in self.info:
            oglutils.texman.getTexture(fruit[3])

        self.hilight = 1
        self.listValid = 0
        self.list = glGenLists(1)

    def tick(self, dT):
        self.action( dT )

    def hiLight(self, state):
        self.hilight = state
        self.listValid = 0

    def draw(self):
        if not self.listValid:
            glNewList(self.list, GL_COMPILE)
            self.render()
            glEndList()
            self.listValid = 1
        glPushMatrix()
        glRotatef(-self.pos, 0, 1, 0)
        glCallList(self.list)
        glPopMatrix()

    def spinTo( self, idx, dt ):
        self.hiLight( 0 )
        self.stopTime = dt+Srnd(self.parent.rollonvar)
        self.v = self.rev*self.parent.getVelocity()
        self.spin_to = idx
        self.action = self.__spinningTo

    def setToIndex(self, index):
        if index < 0 or index >= self.parent.numFruit:
            index = 0
        self.fruitindex = index
        self.pos = self.org = self.parent.indexToPos(index)

    def kickStart(self):
        self.rev = 1.0
        self.hiLight( self.IsHoldOn() )
        self.stopTime = self.parent.getStopTime(self.index)
        self.v = self.rev*self.parent.getVelocity()
        self.action = (self.__rolling, self.__held)[self.IsHoldOn()]

    def getFlags(self, offset=0):
        return self.info[(offset+self.fruitindex)%self.parent.numFruit]

    def winline_fruit(self, offset):
        return self.info[(offset+self.fruitindex)%self.parent.numFruit][0]

    def IsHoldOn(self):
        return truth(self.holdbutton) and self.holdbutton.IsOn()

    def IsStopped(self):
        return (self.action == self.__stopped)

    def render(self):
        normals = self.parent.normals
        texcoords = self.parent.texcoords
        vertices = self.parent.vertices
	glTranslate(self.origin[0], self.origin[1], self.origin[2])
        for i in range(self.parent.numFruit):
            i2 = i*2
            i4 = i*4
            oglutils.texman.getTexture(self.info[i][3]).bind()
            material = self.parent.materials[self.hilight and i==self.fruitindex]
            glMaterialfv(GL_FRONT, GL_EMISSION, material)
	    glBegin(GL_TRIANGLE_FAN)
            glNormal3fv(normals[i2+0])
            glTexCoord2fv(texcoords[i4+0])
            glVertex3fv(vertices[i4+0])
            glTexCoord2fv(texcoords[i4+1])
            glVertex3fv(vertices[i4+1])
            glNormal3fv(normals[i2+1])
            glTexCoord2fv(texcoords[i4+2])
            glVertex3fv(vertices[i4+2])
            glTexCoord2fv(texcoords[i4+3])
            glVertex3fv(vertices[i4+3])
            glEnd()

    def posToFinal(self, pos):
        return float(int(pos+0.5)-int(pos+0.5)%(360/self.parent.numFruit))

    def __setupSettling(self, dT, settlingTime, sfx=1):
        self.v = -self.rev*self.parent.settlev
        dT += self.stopTime
        self.pos += self.v*dT
        self.pos = self.org = self.posToFinal(self.pos)
        self.fruitindex = self.parent.posToIndex(self.pos)
        if sfx == 1:
            self.parent.playWobble()
        self.hiLight( 1 )
        self.settleTime = settlingTime
        self.action = self.__settling

    def __stopped(self, dT):
        pass

    def __rolling(self, dT):
        self.stopTime -= dT
        if self.stopTime <= 0.0:
            self.__setupSettling(dT, self.parent.settlet)
        else:
            self.pos += self.v*dT

    def __held(self, dT):
        self.stopTime -= dT
        if self.stopTime > 0.0:
            return
        self.v = -self.rev*self.parent.settlev/3.0
        self.org = self.pos
        self.settleTime = self.parent.settlet/2.0
        self.action = self.__settling

    def __settling(self, dT):
        self.settleTime -= dT
        if self.settleTime < 0.0:
            self.pos = self.org
            self.fruitindex = self.parent.posToIndex(self.pos)
            self.action = self.__stopped
            return
        amplitude = self.settleTime*1.6
        self.pos = self.org+amplitude*math.sin( (self.parent.settlet-self.settleTime)*40.0 )

    def __spinningTo(self, dT):
        self.stopTime -= dT
        if self.stopTime <= 0.0:
            p = self.pos - self.rev*self.parent.settlev*(dT+self.stopTime)
            i = self.parent.posToIndex( self.posToFinal(p) )
            if i != self.spin_to:
                self.stopTime = 0.01
                self.pos += self.v*dT
                return
            self.__setupSettling(dT, self.parent.settlet)
        else:
            self.pos += self.v*dT


try:
    import psyco
    psyco.bind(Reel.render)
except:
    pass

#
#####################################################################################
#

class ReelUnit:
    def __init__(self, origin=Vector3D.zero):
        self.reels        = []
        self.origin       = origin
        self.numFruit     = 24
        self.winline      = 27
        self.settlet      = 0.5
        self.settlev      = 40.0
        self.preroll      = 1.0
        self.rolldiff     = 0.5
        self.rollonvar    = 0.07
        self.velocity     = -3.0*Rad2Deg(GOLDEN_ANGLE)
        self.speedvar     = 2.0
        self.num_reels    = NUM_REELS
        self.reel_spacing = 0.36
        self.reel_height  = 0.32
        self.reel_radius  = 1.28
        self.materials    = ( (0.0, 0.0, 0.0, 0.0), (1.0, 1.0, 1.0, 1.0), )

	self.createReelGeometry()

        self.stopped = 1

        pos = self.origin
        for i in range(self.num_reels):
            pos = self.origin+Vector3D(0., i*self.reel_spacing, 0.)
            self.reels.append( Reel(self, pos, i) )

	self.reels[0].setToIndex(15)
        self.reels[1].setToIndex( 6)
        self.reels[2].setToIndex( 4)
        self.reels[3].setToIndex( 9)

	self.rules = RuleList(self.num_reels, self.numFruit)

        self.snd_spin   = pygutils.sndman.getSound("ReelSpin.wav")
        self.snd_wobble = pygutils.sndman.getSound("ReelStop.wav")

    def createReelGeometry(self):
	self.vertices	  = []
	self.normals	  = []
	self.texcoords	  = []

	heightby2 = self.reel_height/2.0
        alpha = 0.0
        ptx =  self.reel_radius*math.cos(alpha)
        ptz =  self.reel_radius*math.sin(alpha)
        prevTop = Vector3D(ptx, heightby2, ptz)
        prevBot = Vector3D(ptx, heightby2-self.reel_height, ptz)
        prevNorm = Vector3D(ptx, 0.0, ptz).normal()

        delta = 2.0*math.pi/self.numFruit
        for i in range(self.numFruit):
            self.normals.append(prevNorm)
            self.texcoords.append((1,0))
            self.vertices.append(prevTop)
            self.texcoords.append((0,0))
            self.vertices.append(prevBot)

            alpha += delta
            ptx = self.reel_radius*math.cos(alpha)
            ptz = self.reel_radius*math.sin(alpha)
            prevTop = Vector3D(ptx, heightby2, ptz)
            prevBot = Vector3D(ptx, heightby2-self.reel_height, ptz)
            prevNorm = Vector3D(ptx, 0.0, ptz).normal()

            self.normals.append(prevNorm)
            self.texcoords.append((0,1))
            self.vertices.append(prevBot)
            self.texcoords.append((1,1))
            self.vertices.append(prevTop)

    def getReelHeight(self):
        return self.reel_height

    def getReelRadius(self):
        return self.reel_radius

    def getVelocity(self):
        return self.velocity+Srnd(self.speedvar)

    def getStopTime(self,index):
        return self.preroll+self.rolldiff*float(index+1)+Srnd(self.rollonvar)


    def indexToPos(self,index):
        return (((self.numFruit-1)-index-self.winline)%self.numFruit)*(360.0/self.numFruit)

    def posToIndex(self,pos):
        return (self.numFruit-1)-((int(pos/(360.0/self.numFruit))+self.winline)%self.numFruit)


    def playWobble(self):
        self.snd_wobble.play()

    def kickStart(self):
        self.stopped = 0
        for reel in self.reels:
            reel.kickStart()
        self.snd_spin.play()

    def IsStopped(self):
        return self.stopped

    def getWinLineRuleOffsets(self, offsets):
	winline = []
        for i in range(self.num_reels):
	    winline.append( self.reels[i].winline_fruit(offsets[i]) )
        return self.rules.findWin(winline)

    def getWinLineRule(self):
        return self.getWinLineRuleOffsets( [0]*self.num_reels )

    def getIndex(self, reel):
        return self.reels[reel].fruitindex

    def spinTo(self, reel, index):
        self.stopped = 0
        self.reels[reel].spinTo(index, 0.0)

    def draw(self):
        glPushMatrix()
        glEnable(GL_TEXTURE_2D)                     #texturing on
        glTranslate(self.origin[0], self.origin[1], self.origin[2])
        for reel in self.reels:
            reel.draw()
        glDisable(GL_TEXTURE_2D)                    #texturing off
        glPopMatrix()

    def tick(self, dT):
        stopcount = 0
        for reel in self.reels:
            reel.tick( dT )
            stopcount += reel.IsStopped()

        self.stopped = (stopcount == self.num_reels)
        if self.IsStopped():
            self.snd_spin.stop()

    def event(self,event):
        pass


