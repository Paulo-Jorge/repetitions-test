# This module defines 3D geometrical vectors with the standard
# operations on them.
#
# Written by: Konrad Hinsen 
# URL: http://www.ccl.net/cca/software/SOURCES/PYTHON/HINSEN/Vector.py
# Last revision : 1996-01-26
# Tweaked by jfp: 2001-08-17 - Use until performance is a problem.
#

"""
This module defines three-dimensional geometrical vectors. Vectors support
the usual mathematical operations (v1, v2: vectors, s: scalar): 
  v1+v2           addition
  v1-v2           subtraction
  v1*v2           scalar product
  s*v1            multiplication with a scalar
  v1/s            division by a scalar
  v1.cross(v2)    cross product
  v1.length()     length
  v1.normal()     normal vector in direction of v1
  v1.angle(v2)    angle between two vectors
  v1.x(), v1[0]   first element
  v1.y(), v1[1]   second element
  v1.z(), v1[2]   third element

The module offers the following items for export:
  Vector3D(x,y,z) the constructor for vectors
  isVector3D(x)   a type check function
  zero, one       zero and one (pre-defined constants)
  ex, ey, ez      unit vectors along the x-, y-, and z-axes (pre-defined constants)

Note: vector elements can be any kind of numbers on which the operations
addition, subtraction, multiplication, division, comparison, sqrt, and acos
are defined. Integer elements are treated as floating point elements.
"""

import umath, types

class Vector3D:
    isVector3D = 1

    def __init__(self, x=0., y=0., z=0.):
        self.v = [x,y,z]

    def __repr__(self):
        return "Vector3D(%s,%s,%s)"%(`self.v[0]`,`self.v[1]`,`self.v[2]`)

    def __str__(self):
        return `self.v`

    def __len__(self):
        return len(self.v)

    def __add__(self, other):
        return Vector3D(self.v[0]+other.v[0],self.v[1]+other.v[1],self.v[2]+other.v[2])
    __radd__ = __add__

    def __neg__(self):
        return Vector3D(-self.v[0], -self.v[1], -self.v[2])

    def __sub__(self, other):
        return Vector3D(self.v[0]-other.v[0],self.v[1]-other.v[1],self.v[2]-other.v[2])

    def __rsub__(self, other):
        return Vector3D(other.v[0]-self.v[0],other.v[1]-self.v[1],other.v[2]-self.v[2])

    def __mul__(self, other):
        if isVector3D(other):
            return reduce(lambda a,b: a+b, map(lambda a,b: a*b, self.v, other.v))
        else:
            return Vector3D(self.v[0]*other, self.v[1]*other,self.v[2]*other)

    def __rmul__(self, other):
        if isVector3D(other):
            return reduce(lambda a,b: a+b,map(lambda a,b: a*b, self.v, other.v))
        else:
            return Vector3D(other*self.v[0], other*self.v[1],other*self.v[2])

    def __div__(self, other):
        if isVector3D(other):
            raise TypeError, "Can't divide by a vector"
        else:
            return Vector3D(_div(self.v[0],other), _div(self.v[1],other),_div(self.v[2],other))
            
    def __rdiv__(self, other):
        raise TypeError, "Can't divide by a vector"

    def __cmp__(self, other):
        return cmp(self.v[0],other.v[0]) or cmp(self.v[1],other.v[1]) or cmp(self.v[2],other.v[2])

    def __getitem__(self, index):
        return self.v[index]

# Comparisons with a tolerance

    equalTolerance = 1.0e-8

    def equal( self, other, tol=equalTolerance):
        return (abs(self.v[0]-other.v[0]) <= tol) and\
               (abs(self.v[1]-other.v[1]) <= tol) and\
               (abs(self.v[2]-other.v[2]) <= tol)


    def x(self):
        return self.v[0]

    def y(self):
        return self.v[1]

    def z(self):
        return self.v[2]

    def length(self):
        return umath.sqrt(self*self)

    def normal(self):
        len = self.length()
        if len == 0:
            raise ZeroDivisionError, "Can't normalize a zero-length vector"
        return self/len

    def cross(self, other):
        if not isVector3D(other):
            raise TypeError, "Cross product with non-vector"
        return Vector3D(self.v[1]*other.v[2]-self.v[2]*other.v[1],
                        self.v[2]*other.v[0]-self.v[0]*other.v[2],
                        self.v[0]*other.v[1]-self.v[1]*other.v[0])

    def angle(self, other):
        if not isVector3D(other):
            raise TypeError, "Angle between vector and non-vector"
        cosa = (self*other)/(self.length()*other.length())
        cosa = max(-1.,min(1.,cosa))
        return umath.arccos(cosa)

Vector3D.zero = Vector3D(0.,0.,0.)
Vector3D.one  = Vector3D(1.,1.,1.)
Vector3D.ex   = Vector3D(1.,0.,0.)
Vector3D.ey   = Vector3D(0.,1.,0.)
Vector3D.ez   = Vector3D(0.,0.,1.)

# Type check

def isVector3D(x):
    return hasattr(x,'isVector3D')

# "Correct" division for arbitrary number types

def _div(a,b):
    if type(a) == types.IntType and type(b) == types.IntType:
        return float(a)/float(b)
    else:
        return a/b



