Demo Fruit Machine
==================

A demo of a 3D fruit machine using OpenGL, programmed using Python.

Based on the incomplete DirectX C++ engine used to make
'DirectX Fruit Machine' (see http://www.johnnypops.demon.co.uk/3ddemos/)

Includes complete source-code and scripts for building a stand-alone Windows
executable in a 'cdimage' directory with 'autorun.inf', CD-icon and Readme. 

The source-based version runs on Linux and should work on OS-X.

You *need* OpenGL support to run this program. This means that you will
probably need up-to date drivers for your video-card/onboard-graphics from
the manufacturer. ATI, nVidia, and Intel are the ones I've come across most.

Identify your video hardware, and visit the manufacturers website. I'm not
able to help with this, if you have problems getting OpenGL support working,
complain to Microsoft.


Windows (2K/XP. Might work on 9x/ME: upgrade or use an alternative)
-------------------------------------------------------------------
The source-based version requires the installation of:

Python-2.4.3
  Home     : http://www.python.org/
  Installer: http://www.python.org/ftp/python/2.4.3/python-2.4.3.msi
  Notes    : you'll probably want to add the python directory to the system path.

Numeric-24.2
  Home     : http://numeric.scipy.org/
  Installer: http://kent.dl.sourceforge.net/sourceforge/numpy/Numeric-24.2.win32-py2.4.exe

PyGame-1.7.1
  Home     : http://www.pygame.org/
  Installer: http://www.pygame.org/ftp/pygame-1.7.1release.win32-py2.4.exe

Python Imaging Library (PIL) V1.1.5
  Home     : http://www.pythonware.com/products/pil/
  Installer: http://effbot.org/downloads/PIL-1.1.5.win32-py2.4.exe

PyOpenGL 2.0 V2.0.2.01
  Home     : http://pyopengl.sourceforge.net/
  Installer: http://kent.dl.sourceforge.net/sourceforge/pyopengl/PyOpenGL-2.0.2.01.py2.4-numpy23.exe
  Notes    : the above installer is for Numeric-23, we're using Numeric-24. I didn't have any
             problems, although this version does require the use of 'patches' if used with
             py2exe. I built a new installer against Numeric-24 and incorporating the patches.
             http://www.johnnypops.demon.co.uk/python/PyOpenGL-2.0.2.01.py2.4-numpy24.exe

Psyco-1.5.1 (Optional speed optimisation)
  Home     : http://psyco.sourceforge.net/
  Installer: http://kent.dl.sourceforge.net/sourceforge/psyco/psyco-1.5.1-win-2.4.zip
  Notes    : unzip into a temporary directory and read the INSTALL.txt file (it's easy).


If you want to build a stand-alone Windows executable you'll need:

Py2Exe-0.6.5
  Home     : http://www.py2exe.org/
  Installer: http://kent.dl.sourceforge.net/sourceforge/py2exe/py2exe-0.6.5.win32-py2.4.exe


If you want to build the installer-based version you'll need:

Inno Setup 5 (with macro extensions and additional languages)
  Home     : http://www.jrsoftware.org/isinfo.php
  Installer: http://files.jrsoftware.org/ispack/ispack-5.1.6.exe
  Languages: http://www.jrsoftware.org/files/istrans/


Running the program
-------------------
Spark up a command-prompt. I like the 'Command Prompt Here' utility:
http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/CmdHerePowertoySetup.exe
which provides a right-click menu option on a directory in Windows Explorer.

In the 'demofm' directory type 'python demo-fm.py' without the quotes and hit return.
That should run the program. Spacebar to play, 'Q' or 'Esc' to quit, up/down arrows
rotate the win-line.

If you type 'python demo-fm.py --help' it will show what command-line arguments it accepts:
currently just -f or --fullscreen.


Building the stand-alone
------------------------
In the 'demofm' directory type 'python setup.py py2exe build' without the quotes and
hit return. This should create a 'dist' directory with a 'demo-fm.exe' file in it, and
a few other files. If double-click on 'demo-fm.exe' the program should run.


Building the full installer
---------------------------
In the 'demofm' directory type 'package.bat' without the quotes and hit return. This should 
create a 'cdimage' directory with a 'demo-fm-0.0.1-win32-installer.exe' file in it, and
a few other files. If you double click on 'demo-fm-0.0.1-win32-installer.exe' the
installer should start-up. If you burn it to the root of a CD, it will autoplay and have
a custom icon.


How it works
------------
Most of the action happens in 'reels.py'. The actual drawing is done by the gl* functions
using display lists to speed up rendering. The display lists are re-generated when the 
material changes (which is how the win-line lighting effect is done).

Other scripts handle sounds, images, and window setup.

The whole thing is a state-machine which is efficient, but not very easy to understand 
until you are familiar with the idea.


Notes
-----
If you don't know Python, check out the website. There are some good, free tutorials and
books out there to help you learn it. 

OpenGL is documented (for 'C' programmers) on the web, but the PyOpenGL stuff is very 
similar, and has extensive documentation and examples included.

Pygame is used for sounds, input and window setup, but is a fully-fledged 2D
game programming library in its own right.

PIL is used as an image loader and pre-processor.

Psyco speeds up some of the grunt work, but isn't required.


Good Luck! Have Fun!
John Popplewell

------------------------------------------------------------------
Copyright (C) 2006 John Popplewell, except where stated otherwise.
Web  : http://www.johnnypops.demon.co.uk/
Email: john@johnnypops.demon.co.uk
------------------------------------------------------------------

