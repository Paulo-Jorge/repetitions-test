#!/usr/bin/env python

##############################################################################
#
#   $RCSfile: demo-fm.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: demo-fm.py,v 1.2 2006/06/25 22:12:53 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.2 $"

"""
Based on DirectX FruitMachine demo
"""

file_name = "startup"
demo_name = "fruit-machine"

import os, sys, getopt

import pygame
from pygame.locals  import *
from Vector3D       import *

from OpenGL.GL import *
from OpenGL.GLU import *

import pygutils
import oglutils
import fruitmachine


def setupDisplay(width=640, height=480, fullscreen=0, title=None):
    fs = fullscreen*FULLSCREEN
    pygame.display.set_mode((width,height), OPENGL|DOUBLEBUF|fs)
    if title: pygame.display.set_caption(title)
    glClear(GL_COLOR_BUFFER_BIT)
    pygame.display.flip()

    print glGetString(GL_VENDOR), glGetString(GL_RENDERER),
    print "Version", glGetString(GL_VERSION)


def setupRenderState():
    glFrontFace(GL_CW)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glEnable(GL_LINE_SMOOTH)

    glLightModel(GL_LIGHT_MODEL_LOCAL_VIEWER, 0)
    glLightModel(GL_LIGHT_MODEL_TWO_SIDE, 0)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT , (0.16, 0.16, 0.16, 1.0))

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, (0.7, 0.7, 0.7, 0.0))
    glMaterialfv(GL_FRONT, GL_SPECULAR, (0.0, 0.0, 0.0, 0.0))
    glMaterialfv(GL_FRONT, GL_EMISSION, (0.0, 0.0, 0.0, 0.0))   # background
    glMaterialf(GL_FRONT, GL_SHININESS, 10.0)

    glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.60, 0.60, 0.60))
    glLightfv(GL_LIGHT0, GL_SPECULAR,(0.0, 0.0, 0.0, 0.0))
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)


def setupViewport(width, height):
    glMatrixMode(GL_PROJECTION)
    gluPerspective(50.0, width/float(height), 1.0, 100.0)   #setup lens
    glMatrixMode(GL_MODELVIEW)


def updateCamera(position, lookat, up):
    glLoadIdentity()
    gluLookAt(position[0], position[1], position[2],
              lookat[0], lookat[1], lookat[2],
              up[0], up[1], up[2])


MIN_TIME_SLICE	= 0.02                      # 20 ms shortest slice
MAX_TIME_SLICE	= 0.04                      # 40 ms longest slice
ERR_TIME_SLICE  = 0.25                      # 250ms something is wrong ...
MEAN_TIME_SLICE	= 0.5*(MAX_TIME_SLICE-MIN_TIME_SLICE)+MIN_TIME_SLICE
SLEEP_MULTIPLE  = 2.0

lastTime = 0

def tickWorld( object ):
    global  lastTime

    thisTime = pygame.time.get_ticks()
    dT = (thisTime-lastTime)/1000.0
    if dT < MIN_TIME_SLICE:
        return 0
    if dT < ERR_TIME_SLICE:
        if dT > MAX_TIME_SLICE:
            while 1:
                dT -= MIN_TIME_SLICE
                object.tick( MIN_TIME_SLICE )
                if dT <= MAX_TIME_SLICE:
                    break
        object.tick(dT)
    else:
        pass
    lastTime = thisTime
    return 1


def Usage():
    print "python %s.py [-f|--fullscreen]"%(file_name,)


def get_script_location():
    fullpath = os.path.abspath(sys.argv[0])
    return os.path.split(fullpath)[0]

def set_cwd_to_script_location():
    """Make sure we're in the correct directory."""
    os.chdir(get_script_location())


def main():
    "run the demo"

    set_cwd_to_script_location()
    fullscreen = 0
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f", ["fullscreen"])
        for o, a in opts:
            if o in ("-f", "--fullscreen"):
                fullscreen = 1
    except getopt.GetoptError:
        Usage()
        sys.exit()

    #initialize pygame and setup an opengl display
    pygame.init()

    width  = 800
    height = 600

    setupDisplay(width, height, fullscreen, demo_name.capitalize()+" demo")
    setupViewport(width, height)
    setupRenderState()

    position = Vector3D( 0.0, 0.54, 3.2)
    lookat   = Vector3D( 0.0, 0.54, 0.0)
    up       = Vector3D(-1.0, 0.0,  0.0)
    updateCamera( position, lookat, up )

    glRotate(215.0, 0.0, 1.0, 0.0)

    rootComponent = fruitmachine.FruitMachine()

    doRotate = 1
    fps = pygutils.showFPS(0)

    global lastTime
    lastTime = pygame.time.get_ticks()

    rotation = 0.0
    pygame.key.set_repeat(250, 40)

    while 1:
        #check for quit events
        event = pygame.event.poll()
        if event.type == QUIT:
            break
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                break
            elif event.key == K_q:
                break
            elif event.key == K_UP:
                glRotate(+1.0, 0.0, 1.0, 0.0)
                rotation += 1.0
            elif event.key == K_DOWN:
                glRotate(-1.0, 0.0, 1.0, 0.0)
                rotation += -1.0
            else:
                rootComponent.event(event)
        else:
            rootComponent.event(event)

        glLightfv(GL_LIGHT0, GL_POSITION, (1.0, 1.0, -1.0, 0.0))

        if tickWorld(rootComponent):
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
            rootComponent.draw()
            pygame.display.flip()
            fps.show()

    sys.stdout.write("\n")
    fps.show_average()
    pygame.quit()


if __name__ == '__main__':
    main()

