##############################################################################
#
#   $RCSfile: pygutils.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: pygutils.py,v 1.2 2006/06/25 02:02:20 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.2 $"

import os
import pygame

try:
    import pygame.mixer
    pygame.mixer.pre_init(22050)
except:
    pygame.mixer = None


class dummysound:
    def play(self): pass


class SoundManager:
    def __init__(self, root):
        self.root = root
        self.sounds = {}

    def setRoot(self, root):
        self.root = root

    def getSound(self, filename):
        path = os.path.join(self.root, filename)
        try:
            return self.sounds[path]
        except KeyError:
            newsnd = self.sounds[path] = self.load_sound( path )
            return newsnd

    def getNumSounds(self):
        return len(self.sounds)

    def load_sound(self,file):
        if not pygame.mixer: return dummysound()
        try:
            sound = pygame.mixer.Sound(file)
            return sound
        except pygame.error:
            print 'Warning, unable to load,', file
        return dummysound()


sndman = SoundManager("data")            #simple sound manager



class showFPS:
    def __init__(self, dynamic=0, framecount=25):
        self.dynamic = dynamic
        self.limit = framecount
        self.precount = 2
        self.totalcount = 0
        self.totaltime = 0
        self.reset()

    def reset(self):
        self.count = 0
        self.time = pygame.time.get_ticks()

    def show(self):
        self.count += 1
        if self.count >= self.limit:
            delta_t = pygame.time.get_ticks()-self.time
            fps = (1000.0*self.count)/delta_t
            if self.dynamic:
                print "FPS: %6.2f\r" % (fps,),
            if self.precount:
                self.precount -= 1
            else:
                self.totalcount += self.count
                self.totaltime  += delta_t
            self.reset()

    def show_average(self):
        if self.totaltime:
            fps = (1000.0*self.totalcount)/self.totaltime
            print "Average FPS: %6.2f\r" % (fps,)


