##############################################################################
#
#   $RCSfile: setup.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: setup.py,v 1.3 2006/06/26 01:14:56 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.3 $"

import distutils.core, sys, glob, py2exe

includes = [
]

ignores = [
]

excludes = [
]

scripting = [{
    "script":"demo-fm.py",
    "icon_resources":[(1, "3dbuilder.ico")]
}]

datafiles = [
    ("data", glob.glob("data/*.*")),
]

def setup_console(*args):
    distutils.core.setup(
        script_args = args,
        console     = scripting,
        data_files  = datafiles,
        zipfile     = None,
        options     = {"py2exe": {"includes"    : includes,
                                  "excludes"    : excludes,
                                  "ignores"     : ignores,
                                  "bundle_files": 1,
                                  }},
    )

def setup_windows(*args):
    distutils.core.setup(
        script_args = args,
        windows     = scripting,
        data_files  = datafiles,
        zipfile     = None,
        options     = {"py2exe": {"includes"    : includes,
                                  "excludes"    : excludes,
                                  "ignores"     : ignores,
                                  "bundle_files": 1,
                                 }},
    )

if __name__ == '__main__':
    #setup_windows(*sys.argv[1:])
    setup_console(*sys.argv[1:])

