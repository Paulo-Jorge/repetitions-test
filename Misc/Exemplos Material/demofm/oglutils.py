##############################################################################
#
#   $RCSfile: oglutils.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: oglutils.py,v 1.2 2006/06/25 02:01:43 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.2 $"

import os
import Image
import PngImagePlugin

try:
    from OpenGL.GL import *
except:
    print "'oglutils.py' requires PyOpenGL"
    raise SystemExit

max_dimension = 512

class Texture:
    def __init__(self, filename):
        self.bindname = glGenTextures(1)
        self.makeImage(filename)
        self.bind()
        glTexImage2D( GL_TEXTURE_2D, 0, 3, self.width, self.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, self.image )
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

    def bind(self):
        glBindTexture( GL_TEXTURE_2D, self.bindname )

    def makePOW2(self, dimension):
        pow2 = 1
        while dimension > pow2 and pow2 < max_dimension:
            pow2 <<= 1
        return pow2

    def makeImage(self, filename):
        im = Image.open(filename)
        if im.mode != "RGB":                        # if required,
            im = im.convert("RGB")                  # perform format conversion

        self.width  = self.makePOW2(im.size[0])
        self.height = self.makePOW2(im.size[1])
        if (self.width  != im.size[0]) or \
           (self.height != im.size[1]):             # if required,
            print "Resizing Image: ", filename      # perform resize
            im = im.resize( (self.width, self.height), NEAREST )

        self.image = im.tostring("raw", "RGBX", 0, -1)


class TextureManager:
    def __init__(self, root):
        self.root = root
        self.textures = {}

    def setRoot(self, root):
        self.root = root

    def getTexture(self, filename):
        path = os.path.join(self.root, filename)
        try:
            return self.textures[path]
        except KeyError:
            newtex = self.textures[path] = Texture( path )
            return newtex

    def getNumTextures(self):
        return len(self.textures)


texman = TextureManager("data")            #simple texture manager

