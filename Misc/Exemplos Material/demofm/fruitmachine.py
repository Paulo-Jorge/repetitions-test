##############################################################################
#
#   $RCSfile: fruitmachine.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: fruitmachine.py,v 1.3 2006/06/25 10:50:51 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.3 $"

import sys
import pygame
import pygutils
import reels

from OpenGL.GL import *
from Vector3D import *

def rnd(n):
    import random
    return random.randint(0, n-1)

def pounds(n):
    return int(n/100)

def pence(n):
    return n%100

class FruitMachine:
    def __init__(self, origin=Vector3D.zero):
        self.initCash = 1000                # Constants (from machine definitions?)
        self.stake    = 20
        self.holdOdds = 50
        self.holdAfterWin = 0
        self.holdAfterWinMax = 100
        self.holdOddsAfterWin = 20
        self.holdSuppressAfterWin = 1

        self.kids = []                      # Component (base class) stuff ?
        self.origin = origin

        self.totalPaidIn = 0                # Stats.
        self.totalPaidOut = 0
        self.totalPlays = 0

        self.credits = self.initCash
        self.winnings = 0
        self.goCounter = 0
        
        self.state = self.__stopped
        self.autoOn = 0
        self.autoGamble = 0
        self.autoHold = 0
        self.autoPlay = 0
        self.holdOn = 0

        self.win = 0

        self.winVoice = pygame.mixer.find_channel()
        self.demoVoice = pygame.mixer.find_channel()

        self.holdStream = None
        self.winStream = None
        self.demoStream = None

        self.reels = reels.ReelUnit()       # Assemble components
        self.kids.append( self.reels )

        self.__enterStoppedState()          # Initialize state-machine

        self.__showCash()

    def draw(self):
        glPushMatrix()
        glTranslate(self.origin[0], self.origin[1], self.origin[2])
        for kid in self.kids:
            kid.draw()
        glPopMatrix()


    def tick(self, dT):
        for kid in self.kids:               # Component::Tick()
            kid.tick( dT )
        self.state()

    def event(self,event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.__startPlayCycle()

        return self.winVoice.get_busy()

    def IsReady(self):
        return (self.state == self.__stopped) and self.InCredit() and not self.soundsPlaying()

    def InCredit(self):
        return self.credits >= self.stake

    def __startPlayCycle(self):
        if self.state == self.__rolling: return

        self.state = self.__rolling
        self.credits -= self.stake
        self.totalPaidIn += self.stake
        self.goCounter += 1
        self.totalPlays += 1
        self.__showCash()
        self.reels.kickStart()

    def __holdInhibit(self):
        return self.holdSuppressAfterWin and self.winVoice.get_busy()

    def __enterStoppedState(self):
        self.state = self.__stopped
        self.autoPlay = self.autoOn

        if self.holdOn and self.InCredit() and not self.__holdInhibit():
            pass

        if not self.InCredit() and self.winnings:
            pass

        if not self.InCredit() and not self.winnings:
            pass


    def __autoPlay(self):
        if not self.IsReady(): return
        if (not self.holdOn) and self.autoPlay:
            self.__startPlayCycle()
            self.autoPlay = 0


    def __autoStarting(self):
        if self.autoHold:
            self.__autoHold()
        elif self.autoPlay:
            self.__autoPlay()


    def __stopped(self):
        if self.autoOn:
            self.__autoStarting()


    def __rolling(self):
        if self.reels.IsStopped():
            self.state = self.__winCheck


    def __winCheck(self):
        self.win = 0
        self.holdOn = 0
        rule = self.reels.getWinLineRule()
        if rule:
            self.win = rule.getTotalPrize()
            self.winStream = rule.getStream()
            if self.holdAfterWin and self.win <= self.holdAfterWinMax:
                self.holdOn = (rnd(100) <= self.holdOddsAfterWin)
        else:
            self.holdOn = (rnd(100) <= self.holdOdds)

        if self.win:
            if self.winStream:
                self.winVoice.play(self.winStream)
            self.winnings += self.win
            self.totalPaidOut += self.win
            self.__showWinnings()

        self.__enterStoppedState()


    def __showCash(self):
        sys.stdout.write( "Credits: $%2d.%02d Winnings: $%2d.%02d Go's: %4d\r"%(
                    pounds(self.credits),pence(self.credits),
                    pounds(self.winnings),pence(self.winnings),self.goCounter) )
	sys.stdout.flush()

    def __showWinnings(self):
        self.__showCash()



