##############################################################################
#
#   $RCSfile: package.py,v $
#
#   Copyright (C) 2006 by John Popplewell
#   Email:   john@johnnypops.demon.co.uk
#   Website: http://www.johnnypops.demon.co.uk/
#
#   $Id: package.py,v 1.4 2006/06/26 01:16:17 jfp Exp $
#
##############################################################################

_FILE_REVISION      = "$Revision: 1.4 $"

import os, sys, shutil

OUTPUT_DIR_NAME = "cdimage"
OUTPUT_DIR      = os.path.abspath(OUTPUT_DIR_NAME)

BDIST_DIR       = "dist"

IIS_EXE_NAME    = "iscc.exe"
ISS_EXE         = os.path.abspath("/Program Files/Inno Setup 5/"+IIS_EXE_NAME)

from version import config

config["OUTPUT_DIR"] = OUTPUT_DIR
config["BDIST_DIR"] = BDIST_DIR

def mkdir(path):
    if not os.path.exists(path):
        os.mkdir(path)

def rmtree(path):
    if os.path.exists(path):
        shutil.rmtree(path)

crlf_choices = ["CR", "CRLF", "LF"]
line_endings = ['\r', '\r\n', '\n']

def ConvertCRLF(str, crlf):
    temp = str.replace('\r\n','\n') # CRLF -> LF
    temp = temp.replace('\r','\n')  # CR   -> LF
    return temp.replace('\n', line_endings[crlf])

def ConvertNative(str):
    crlf = 2
    if sys.platform == 'win32':
        crlf = 1
    return ConvertCRLF(str, crlf)

rmtree(OUTPUT_DIR)

print "Packaging program for '%s'"%config["APP_TITLE"]
print

if not os.path.exists(ISS_EXE):
    print "This packager requires the 'Inno Setup' installer-builder."
    print "If you already have 'Inno Setup' installed, perhaps this path is wrong:"
    print
    print "ISS_EXE='%s'"%ISS_EXE
    print
    print "See line 6 of this script if that is the case."
    print "Otherwise, download 'Inno Setup' from here: http://www.innosetup.com/"
    print
    print "Packaging: FAILED. Problem finding 'Inno Setup'."
    sys.exit(1)

import setup

try:
    mkdir(OUTPUT_DIR)
except OSError:
    print "Packaging: %s"%sys.exc_info()[1]
    print "Packaging: FAILED. Problem creating output directory."
    sys.exit(2)

try:
    print "Building Stand-alone Python Program using py2exe:"
    setup.setup_windows("--quiet", "py2exe")
except SystemExit, (msg, ):
    print "Packaging: %s"%msg
    print "Packaging: FAILED. Problem with py2exe."
    sys.exit(3)

try:
    os.remove(os.path.join(BDIST_DIR, "config.ini"))
except OSError:
    pass


iss_setup_section = """
#define AppPublisher    "%(APP_PUBLISHER)s"
#define AppPublisherURL "%(APP_PUBLISHER_URL)s"

#define AppName         "%(APP_TITLE)s"
#define AppDirName      "%(APP_PREFIX)s"
#define AppPrefix       "%(PKG_PREFIX)s"
#define AppRegKey       AppDirName
#define AppRegValue     "Install_Dir"
#define AppRegVersion   "Version"
#define AppSuffix       "%(APP_SUFFIX)s"
#define AppReadme       "%(APP_README)s"
#define AppLicence      "%(APP_LICENCE)s"

#define AppGroupName    AppName
#define AppExe          AppPrefix+".exe"

#define BdistDir        "%(BDIST_DIR)s"
#define AppOutputDir    "%(OUTPUT_DIR)s"
#define AppVersion      "%(APP_VERSION)s"
"""%(config)

ISS_SCRIPT_IN   = "config.iss"
ISS_SCRIPT      = "package.iss"

print
print "Creating installer include:"
fo = open(ISS_SCRIPT_IN, "wb")
fo.write(ConvertCRLF(iss_setup_section, 1))
fo.close()

shutil.copy2(config["APP_README"], os.path.join(BDIST_DIR, config["APP_README"]))
shutil.copy2(config["APP_LICENCE"], os.path.join(BDIST_DIR, config["APP_LICENCE"]))

print
print "Building Windows Installer using 'Inno Setup':"
print "Please wait, this may take some time ..."
print
print ISS_SCRIPT
print ISS_EXE
res = os.spawnl(os.P_WAIT, ISS_EXE, IIS_EXE_NAME, ISS_SCRIPT)
if res:
    print "Packaging: FAILED. Problem with 'Inno Setup'."
    sys.exit(4)

autorun_inf = r"""[autorun] 
open=%s-%s-%s.exe 
icon=%s
shell\readit\command=notepad %s
shell\readit=Read &Me
"""%(config["PKG_PREFIX"], config["APP_VERSION"], config["APP_SUFFIX"], config["APP_ICON"], config["APP_README"])

print
print "Creating autorun.inf:"
fp = open(os.path.join(OUTPUT_DIR, "autorun.inf"), "w")
fp.write(autorun_inf)
fp.close()

try:
    print "Copying additional files:"
    shutil.copy2(config["APP_ICON"], os.path.join(OUTPUT_DIR, config["APP_ICON"]))
    shutil.copy2(config["APP_README"], os.path.join(OUTPUT_DIR, config["APP_README"]))
    shutil.copy2(config["APP_LICENCE"], os.path.join(OUTPUT_DIR, config["APP_LICENCE"]))
except:
    print "Packaging: %s"%sys.exc_info()[1]
    print "Packaging: FAILED. Problem copying additional files into '%s'."%OUTPUT_DIR
    sys.exit(5)


print
print "Packaging: completed successfully."
print "Packaging: the directory '%s'"%OUTPUT_DIR
print "Packaging: contains all the files required to make a CD."
print

